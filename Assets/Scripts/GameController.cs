﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public float smooth = 5.0f;
    private float currRotZ = 0;
    private bool isMove = false;

    public Button chantButton;

    public int chantNumber;
    public GameObject[] chantVersion;
    public AudioSource[] chant;

    public int chantFxNumber;
    public GameObject[] chantObject;
    public Animation[] chantAnim;
    private int fxCount = 0;

    public bool IsRainFx;
    public bool IsThunderFx;
    public AudioSource rainSound;
    public AudioSource thunderSound;

    void Start()
    {
        int totalChant = chantVersion[chantNumber].transform.childCount;
        chant = new AudioSource[totalChant];
        for (int i = 0; i < totalChant; i++)
        {
            chant[i] = chantVersion[chantNumber].transform.GetChild(i).GetComponent<AudioSource>();
        }

        int totalChantObject = chantObject[chantFxNumber].transform.childCount;
        chantAnim = new Animation[totalChantObject];
        for (int i = 0; i < totalChantObject; i++)
        {
            chantAnim[i] = chantObject[chantFxNumber].transform.GetChild(i).GetComponent<Animation>();
        }

        if (IsRainFx)
            rainSound.Play();

        if (IsThunderFx)
            thunderSound.Play();
        
    }

    void Update()
    {
        if (isMove)
        {
            Quaternion target = Quaternion.Euler(0, 0, currRotZ - 11.25f);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * smooth);

            if (transform.rotation.z < (target.z + 0.0001f))
            {
                transform.rotation = target;

                isMove = false;

                currRotZ = transform.localEulerAngles.z;
                if (currRotZ > 180.0f)
                {
                    currRotZ = currRotZ - 180.0f;
                    transform.Rotate(0, 0, currRotZ);
                }

                chantButton.interactable = true;
            }
        }
    }

    public void OnChantButton ()
    {
        currRotZ = transform.localEulerAngles.z;
        isMove = true;
        chantButton.interactable = false;

        for (int i = 0; i < chant.Length; i++)
        {
            if (!chant[i].isPlaying)
            {
                chant[i].Play();
                break;
            }
        }

        chantAnim[fxCount].Play();
        fxCount++;
        if (fxCount == chantAnim.Length)
        {
            fxCount = 0;
        } 

    }

}
